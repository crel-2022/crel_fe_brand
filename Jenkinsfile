pipeline {

  agent any
  
  parameters {
    string(name: 'SSH_USER', defaultValue: '', description: 'SSH user for the remote server')
    string(name: 'SSH_SERVER_IP', defaultValue: '', description: 'IP address of the remote server')
    string(name: 'SSH_PRIVATE_KEY', defaultValue: '', description: 'Private SSH key for accessing the remote server')
    string(name: 'DOCKER_USER', defaultValue: '', description: 'Docker Hub username')
    string(name: 'DOCKER_TOKEN', defaultValue: '', description: 'Docker Hub access token')
  }

  stages {
    stage('Build') {
      when {
        branch 'main'
      }
	  agent {
		docker {
			image 'node:14-alpine'
			args '-u root:root'
		}
	  }
      steps {
        sh 'apk add --no-cache openssh-client'
        sh 'apk add bash'

        // Add the SSH key to the agent store
        sh 'eval $(ssh-agent -s)'
        sh "bash -c 'ssh-add <(echo \"\$SSH_PRIVATE_KEY\")'"

        // Create the ~/.ssh folder
        sh 'mkdir -p ~/.ssh'

        // Scan for the SSH host key for the server IP address
        // Add the results to the known_hosts file
        sh 'ssh-keyscan -H $SSH_SERVER_IP >> ~/.ssh/known_hosts'

        // Change the permissions of the known_hosts file
        sh 'chmod 644 ~/.ssh/known_hosts'

        // Clone the project from Git to the remote server
        sh "ssh $SSH_USER@$SSH_SERVER_IP 'sudo rm -rf /home/$SSH_USER/CREL-Brand'"
        sh "ssh $SSH_USER@$SSH_SERVER_IP 'sudo mkdir /home/$SSH_USER/CREL-Brand'"
        sh "ssh $SSH_USER@$SSH_SERVER_IP 'sudo git clone https://gitlab.com/crel-2022/crel_fe_admin.git /home/$SSH_USER/CREL-Brand'"

        // Build the Docker image on the remote server
        sh "ssh $SSH_USER@$SSH_SERVER_IP 'cd /home/$SSH_USER/CREL-Brand && docker build --no-cache -t $DOCKER_USER/crel_2022:brand .'"

        // Push Docker images to Docker Hub
        sh "ssh $SSH_USER@$SSH_SERVER_IP 'docker login -u $DOCKER_USER -p $DOCKER_TOKEN'"
        sh "ssh $SSH_USER@$SSH_SERVER_IP 'docker push $DOCKER_USER/crel_2022:brand'"
      }
    }

    stage('Deploy') {
      when {
        branch 'main'
      }
	  agent {
        docker {
          image 'alpine:latest'
        }
      }
      steps {
        sh 'apk add --no-cache openssh-client'
        sh 'apk add bash'

        // Add the SSH key to the agent store
        sh 'eval $(ssh-agent -s)'
        sh "bash -c 'ssh-add <(echo \"\$SSH_PRIVATE_KEY\")'"

        // Create the ~/.ssh folder
        sh 'mkdir -p ~/.ssh'

        // Scan for the SSH host key for the server IP address
        // Add the results to the known_hosts file
        sh 'ssh-keyscan -H $SSH_SERVER_IP >> ~/.ssh/known_hosts'

        // Change the permissions of the known_hosts file
        sh 'chmod 644 ~/.ssh/known_hosts'

        // Deploy the Docker image using docker-compose
        sh 'ssh $SSH_USER@$SSH_SERVER_IP "docker-compose -f docker-compose.yaml up -d crel-brand"'
		}
	}
  }
}