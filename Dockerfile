# build stage
FROM node:14-alpine AS build-brand
WORKDIR /app
COPY package*.json ./
RUN npm i
COPY . .
RUN npm i -g @angular/cli
RUN npm run build

# production stage
FROM nginx:alpine
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-brand /app/dist/ /usr/share/nginx/html